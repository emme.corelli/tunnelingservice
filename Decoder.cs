﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;

namespace TunnelingService
{
    public class Decoder
    {
        static private readonly byte[] __ivc = new byte[16]; // PUT HERE YOUR 16 BYTES OF   IV
        static private readonly byte[] __key = new byte[16]; // PUT HERE YOUR 16 BYTES OF  KEY
        static private readonly byte[] __cod = new byte[16]; // PUT HERE YOUR 16 BYTES OF CODE

        static public byte[] Encrypt(byte[] plain)
        {
            byte[] encrypted;
            byte[] _time = BitConverter.GetBytes((Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
            byte[] msg = new byte[_time.Length + __cod.Length + plain.Length];
            Array.Copy(_time, 0, msg, 0, _time.Length);
            Array.Copy(__cod, 0, msg, _time.Length, __cod.Length);
            Array.Copy(plain, 0, msg, _time.Length + __cod.Length, plain.Length);

            Debug.WriteLine(msg.ToString());

            using (Aes aesAlg = Aes.Create())
            {
                // .Mode = CipherMode.CBC;
                aesAlg.KeySize = 128;
                // .BlockSize = 128;
                // .FeedbackSize = 128;
                // .Padding = PaddingMode.PKCS7;
                aesAlg.Key = __key;
                aesAlg.IV = __ivc;

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        csEncrypt.Write(msg, 0, msg.Length);
                        csEncrypt.Close();
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return encrypted;
        }

        static public byte[] Decrypt(byte[] encrypted)
        {

            // Declare the string used to hold 
            // the decrypted text. 
            byte[] plain = null;

            // Create an Aes object 
            // with the specified key and IV. 
            using (Aes aesAlg = Aes.Create())
            {
                // .Mode = CipherMode.CBC;
                aesAlg.KeySize = 128;
                // .BlockSize = 128;
                // .FeedbackSize = 128;
                // .Padding = PaddingMode.PKCS7;
                aesAlg.Key = __key;
                aesAlg.IV = __ivc;

                // Create a decrytor to perform the stream transform.
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (var msDecrypt = new MemoryStream(encrypted))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        int len = csDecrypt.Read(encrypted, 0, encrypted.Length);
                        csDecrypt.Close();

                        plain = msDecrypt.ToArray();
                        plain = plain.Take(len).Skip(20).ToArray();
                    }
                }
            }
            return plain;
        }

    }
}
