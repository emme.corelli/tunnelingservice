﻿using System;
using System.ServiceProcess;


namespace TunnelingService
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                TunnelingService service = new TunnelingService();
                service.TestStartupAndStop(args);
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new TunnelingService()
                };
                var log = LoggerHelper.GetLogger();
                log.Info("TunnelingService created");
                ServiceBase.Run(ServicesToRun);
                // log.Info("TunnelingService running");
            }
        }
    }
}
