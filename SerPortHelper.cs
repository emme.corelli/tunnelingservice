﻿using System;
using System.Diagnostics;
using System.IO.Ports;

namespace TunnelingService
{
    class SerPortHelper
    {
        private const int SIZE = 1024;

        static public byte[] ReadBuffer(SerialPort port, int ReadTimeout = 5000)
        {
            int idx = 0;
            byte[] bytes_read = new byte[SIZE];

            try
            {
                port.ReadTimeout = ReadTimeout;
                //while (idx < bytes_read.Length)
                //{
                //    byte Byte = (byte)port.ReadByte();
                //    bytes_read[idx++] = Byte;
                //    port.ReadTimeout = 100;
                //}
                idx += port.Read(bytes_read, 0, 1);
                while (port.BytesToRead > 0)
                    idx += port.Read(bytes_read, idx, bytes_read.Length-idx);
            }
            catch (TimeoutException)
            {
                // Skip: no message received
                //        L'operazione non è stata completata prima dello scadere del periodo di timeout.
                //        - oppure -
                //        Non sono stati letti byte.
                Debug.WriteLine($"{DateTime.Now} SerPortHelper.ReadBuffer TimeoutException (end of msg). {port.ReadTimeout}");
            }
            catch (Exception ex)
            {
                //Eccezioni
                //    InvalidOperationException
                //        La porta specificata non è aperta.
                throw ex;
            }
            finally
            {
                port.ReadTimeout = ReadTimeout;
            }

            if (idx == 0)
            {
                return null;
            }
            var buffer = new byte[idx];
            Array.Copy(bytes_read, 0, buffer, 0, idx);
            return buffer;
        }

        static public byte[] WriteAndReadBuffer(SerialPort port, byte[] buffer)
        {
            byte[] bytes = null;
            try
            {
                port.Open();
                port.Write(buffer, 0, buffer.Length);
                bytes = ReadBuffer(port, 500);
            }
            catch { }
            finally
            {
                if ((port != null) && port.IsOpen)
                    port.Close();
            }

            return bytes;
        }
    }
}
