﻿using System;
using System.Diagnostics;
using System.IO;
using YamlDotNet.RepresentationModel;

namespace TunnelingService
{
    class YamlHelper
    {

        static public YamlMappingNode LoadFromFile(string filename)
        {
            var log = LoggerHelper.GetLogger();
            YamlMappingNode doc = null;

            try
            {
                log.Info($"Yaml file is: {filename}");
                Debug.WriteLine(filename);
                if (string.IsNullOrEmpty(filename) || !File.Exists(filename))
                {
                    // Exception
                    Debug.WriteLine($"Yaml file {filename} not found!");
                    log.Fatal($"Yaml file {filename} not found!");
                    throw new FileNotFoundException($"Error to find {filename}");
                }

                log.Trace($"Reading Yaml file ...");
                var str = new StringReader(File.ReadAllText(filename));
                var yaml = new YamlStream();

                log.Trace($"Decoding Yaml file ...");
                yaml.Load(str);

                log.Trace($"Extracting Yaml rootnode ...");
                doc = (YamlMappingNode)yaml.Documents[0].RootNode;
            }
            catch
            {
                log.Fatal($"'YamlHelper.LoadFromFile' something gone wrong !!!");
            }
            return doc;
        }

        static public string GetStr(YamlMappingNode yaml, string key, string def = "")
        {
            var log = LoggerHelper.GetLogger();
            string value = def;

            try
            {
                if (yaml == null)
                {
                    log.Fatal($"Yalm document model is null!");
                    throw new ArgumentNullException("Yalm document model is null");
                }
                log.Debug($"Reading Yaml[{key}]");
                if (yaml.Children.ContainsKey(new YamlScalarNode(key)) == false)
                {
                    log.Warn($"Yaml[{key}] is null! Return default value '{def}'.");
                    return value;
                }
                log.Trace($"Reading YamlChildren[{key}] ...");
                var node = yaml.Children[new YamlScalarNode(key)];
                value = node.ToString();
                log.Debug($"Yaml[{key}]={value}");
            }
            catch
            {
                log.Fatal($"'YamlHelper.GetStr' something gone wrong !!!");
            }
            return value;
        }

        static public int GetInt(YamlMappingNode yaml, string key, int def = 0)
        {
            int value = def;
            
            if (Int32.TryParse(GetStr(yaml, key, def.ToString()), out int num))
            {
                value = num;
            }
            return value;

        }


    }
}
