﻿using NLog;
using System.IO;


namespace TunnelingService
{
    class LoggerHelper
    {
        private static Logger logger = null;

        static public Logger GetLogger()
        {
            if (logger != null)
                return logger;

            logger = LogManager.GetCurrentClassLogger();
            return logger;
        }

        static public void SetConfig(string rootPath, string logPath, string level)
        {
            var config = new NLog.Config.LoggingConfiguration();

            if (logPath.StartsWith(@"..") || logPath.StartsWith(@".\"))
            {
                logPath = Path.GetFullPath(Path.Combine(rootPath, logPath));
            }

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile")
            {
                Name = "TunnelingService",
                CreateDirs = true,
                Layout = "${longdate} ${level:uppercase=true:padding=-5} ${message}${exception:format=ToString}",
                FileName = string.Format("{0}\\{1}", logPath, "TunnelingService.log"),
                ArchiveDateFormat = "yyyyMMdd",
                ArchiveFileName = string.Format("{0}\\{1}", logPath, "TunnelingService-{#}.log"),
                ArchiveEvery = NLog.Targets.FileArchivePeriod.Day,
                ArchiveNumbering = NLog.Targets.ArchiveNumberingMode.Date,
                MaxArchiveFiles = 5,
                ConcurrentWrites = true,
            };

            // Rules for mapping loggers to targets            
            config.AddRule(LogLevel.FromString(level), LogLevel.Fatal, logfile);

            // Apply config           
            LogManager.Configuration = config;
        }
    }
}
