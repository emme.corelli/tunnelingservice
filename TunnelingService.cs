﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.Serialization;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace TunnelingService
{
    public partial class TunnelingService : ServiceBase
    {
        private readonly string phSerialPort;
        private readonly string brSerialPort;
        private readonly bool startService = false;
        private readonly int startDelay;
        private readonly int startTries;

        private Thread thread = null;
        private bool running = true;

        private readonly EventWaitHandle eventWait = new EventWaitHandle(false, EventResetMode.ManualReset);

        private void ThreadTask()
        {
            var log = LoggerHelper.GetLogger();

            log.Info("ThreadTask created.");

            log.Debug("bridgePort created.");
            SerialPort bridgePort = new SerialPort(brSerialPort, 115400, Parity.None, 8, StopBits.One);
            log.Debug("phisycalPort created.");
            SerialPort maestroPort = new SerialPort(phSerialPort, 115400, Parity.None, 8, StopBits.One);

            int tries = startTries;
            while (tries-- > 0)
            {
                try
                {
                    log.Debug("bridgePort opened.");
                    bridgePort.Open();
                    // ExitCode = 0;
                    running = true;
                    break;
                }
                catch (Exception ex)
                {
                    log.Fatal($"'ThreadTask' something gone wrong {ex}!!!");
                    // ExitCode = 2;
                    running = false;
                }

                if ((running == false) && (startDelay > 0) && (tries > 0))
                {
                    log.Debug($"ThreadTask delayed {tries}:{startDelay} Sec.");
                    Thread.Sleep(startDelay * 1000);
                }
            }

            eventWait.Set();

            while (running)
            {
                try
                {
                    log.Trace("bridgePort RdBuffer ...");
                    var data_received = SerPortHelper.ReadBuffer(bridgePort, 30000);

                    if (data_received == null)
                    {
                        log.Trace("bridgePort RdBuffer=null ...");
                        Thread.Sleep(500);
                        continue;
                    }
                    log.Debug($"bridgePort RdBuffer={BitConverter.ToString(data_received)}");

                    // Encrypt
                    var encrypted = Decoder.Encrypt(data_received);
                    log.Trace($"Encrypted={BitConverter.ToString(encrypted)} ...");

                    // Send and receive
                    var buffer = SerPortHelper.WriteAndReadBuffer(maestroPort, encrypted);
                    if (buffer == null)
                    {
                        log.Warn($"physicalPort RdBuffer==null");
                        continue;
                    }

                    log.Trace($"physicalPort RdBuffer={BitConverter.ToString(buffer)} ...");

                    // Decrypt
                    var data_to_send = Decoder.Decrypt(buffer);
                    log.Trace($"Decrypted={BitConverter.ToString(data_to_send)}");

                    // Reply
                    bridgePort.Write(data_to_send, 0, data_to_send.Length);
                    log.Debug($"bridgePort WrBuffer={BitConverter.ToString(data_to_send)}");

                    Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    log.Error($"'ThreadTask' something gone wrong {ex}!!!");
                }
                running &= bridgePort.IsOpen;
            }
            if ((bridgePort != null) && bridgePort.IsOpen)
            {
                bridgePort.Close();
            }

            log.Warn($"ThreadTask set running to {running}");
            // Stop();
        }

        public TunnelingService()
        {
            Logger log = null;
            InitializeComponent();

            try
            {
                var fileyaml = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\config.yaml"));
                var yaml = YamlHelper.LoadFromFile(fileyaml);

                // Set logger
                var logPath = YamlHelper.GetStr(yaml, "service_log_path", @".\log");
                var logVerb = YamlHelper.GetStr(yaml, "service_log_level", "DEBUG");

                LoggerHelper.SetConfig(AppDomain.CurrentDomain.BaseDirectory, logPath, logVerb);
                log = LoggerHelper.GetLogger();
                log.Info($"Service logging started: {logVerb} --------------------------------------");
                Debug.WriteLine($"service LOG LEVEL is set to'{logVerb}'");

                log.Info("The version is: {0}", typeof(TunnelingService).Assembly.GetName().Version);
                Debug.WriteLine("The version is: {0}", typeof(TunnelingService).Assembly.GetName().Version);

                brSerialPort = YamlHelper.GetStr(yaml, "service_bridge_port");
                Debug.WriteLine($"serial COM of bridge 2 MAESTRO2 is '{brSerialPort}'");
                log.Info($"serial COM of bridge 2 MAESTRO2 is '{brSerialPort}'");

                phSerialPort = YamlHelper.GetStr(yaml, "service_direct_port");
                Debug.WriteLine($"serial COM of MAESTRO2 board is '{phSerialPort}'");
                log.Info($"serial COM of MAESTRO2 board is '{phSerialPort}'");

                startService = YamlHelper.GetStr(yaml, "service_run").ToUpper().Equals("TRUE");
                log.Info($"Start service is setted to '{startService}'");

                startDelay = YamlHelper.GetInt(yaml, "service_delay");
                log.Info($"Start service delayed {startDelay} Sec.");

                startTries = YamlHelper.GetInt(yaml, "service_tries");
                startTries = startTries > 0 ? startTries : 1;

                log.Info($"Start service tries {startTries}.");

            }
            catch (Exception ex)
            {
                if (log != null)
                    log.Fatal($"'TunnelingService()' something gone wrong {ex}!!!");
            }
        }


        protected override void OnStart(string[] args)
        {
            var log = LoggerHelper.GetLogger();

            running = false;
            if (!startService)
            {
                Debug.WriteLine("Service disabled. Not started.");
                log.Warn("Service disabled. Not started.");
                return;
            }
            running = true;
            eventWait.Reset();
            Thread thread = new Thread(new ThreadStart(this.ThreadTask))
            {
                IsBackground = true
            };
            log.Debug("ThreadTask loaded");
            thread.Start();
            Thread.Sleep(1000);


            eventWait.WaitOne();
            if (running)
            {
                log.Info("ThreadTask started");
                ExitCode = 0;
                return;
            }

            log.Fatal("ThreadTask not started");
            thread = null;
            ExitCode = 2;
            Stop();
        }


        protected override void OnStop()
        {
            var log = LoggerHelper.GetLogger();

            log.Warn("Service will be stopped.");
            running = false;
            if (thread == null)
            {
                log.Warn("Service is null. Stopped.");
                return;
            }

            log.Debug("Await service dead.");
            while (thread.IsAlive)
            {
                log.Trace("Await ...");
                Thread.Sleep(500);
                thread.Abort();
            }
            log.Warn("Service stopped.");
            thread = null;
        }

        internal void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.WriteLine("Premi CR");
            Console.ReadLine();
            this.OnStop();
        }
    }


    [Serializable]
    internal class Exeption : Exception
    {
        public Exeption()
        {
        }

        public Exeption(string message) : base(message)
        {
        }

        public Exeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected Exeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
